const bodyEl = document.getElementsByTagName('body')[0];
let colorVal = 0;

const animate = function() {
	let colorValues = bodyEl.style.background = `rgb(${colorVal},${colorVal},${colorVal})`;
	colorVal++;
	if(colorVal <= 255) {
		requestAnimationFrame(animate);
	}
}
requestAnimationFrame(animate);