// create api-key.js file with const API_KEY="your_api_key" in this same directory to use

// Calls the New York Times API to find the best selling hardcover fiction books for the date

// Collect html input upon clicking Get Books button
let yearEl;
let monthEl;
let dateEl;

const submitEl = document.getElementsByTagName('form')[0];
submitEl.addEventListener('submit', function(e) {
	yearEl = document.getElementsByName('year')[0].value;
	monthEl = document.getElementsByName('month')[0].value;
	dateEl = document.getElementsByName('date')[0].value;
	console.log(`${yearEl}-${monthEl}-${dateEl}`);
	e.preventDefault();

	// Fetch from NYTimes
	let date = `${yearEl}-${monthEl}-${dateEl}`;
	const BASE_URL = `https://api.nytimes.com/svc/books/v3/lists/${date}/hardcover-fiction.json`;
	const url = `${BASE_URL}?api-key=${API_KEY}`;

	fetch(url)
		.then(function(response) {
		return response.json();
	})
		.then(function(responseJson) {
		console.log(responseJson)
		let results = responseJson.results.books;
		let resultsTitle = [];
		let resultsAuthor = [];
		let resultsDescription = [];
		for (let i = 0; i < results.length; i++) {
			resultsTitle.push(results[i].title);
			resultsAuthor.push(results[i].author);
			resultsDescription.push(results[i].description);
		}
		
		// Check in browser console
		console.log(resultsTitle);
		console.log(resultsAuthor);
		console.log(resultsDescription);
		
		// Display results in html
		const itemDisplayCount = 5;
		const bookTitleEl = document.getElementsByClassName('book-title');
		const bookAuthorEl = document.getElementsByClassName('book-author');
		const bookDescriptionEl = document.getElementsByClassName('book-description');
		
		for (i = 0; i < itemDisplayCount; i++) {
			bookTitleEl[i].innerHTML = `Title: ${resultsTitle[i]}`;
			bookAuthorEl[i].innerHTML = `Author: ${resultsAuthor[i]}`;
			bookDescriptionEl[i].innerHTML = `Description: ${resultsDescription[i]}`;
		}
	})
});