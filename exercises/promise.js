// Create a new Practice
let promisePractice = new Promise(function(resolve, reject) {
	setTimeout(function() {
		const randomVal = Math.random();
		if (Math.random() > 0.5) {
			resolve();
		} else {
			reject();
		}
	}, 1000);
});

let secondPromise = promisePractice.then(function() {
	console.log('success');
});

let thirdPromise = secondPromise.catch(function() {
	console.log('fail');
});

let fourthPromise = thirdPromise.then(function() {
	console.log('complete');
});