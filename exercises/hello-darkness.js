let bodyTag = document.getElementsByTagName('body')[0];
let colorVal = 255;

const darkenBackground = function() {
	if (colorVal >= 0) {
		let colorSpec = bodyTag.style.background = `rgb(${colorVal},${colorVal},${colorVal})`;
		console.log(colorVal)
		colorVal -= 1;
	}
}

const darkBackGroundInterval = setInterval(
	darkenBackground, 500);